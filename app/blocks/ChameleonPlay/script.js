var chameleon = document.getElementsByClassName("chameleon_js")[0],
    lampButton = document.getElementsByClassName("lamp__button_js")[0],
    lamp = document.getElementsByClassName("lamp_js")[0];

if (chameleon && lampButton && lamp) {
    setTimeout(onOf, 11000);
    lampButton.onclick = onOf;
    function onOf() {
        lamp.classList.toggle("lamp_on");
        chameleon.classList.toggle("chameleon_on");
    }
}




