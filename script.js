const ejs = require('ejs'),
    fs = require('fs'),
    sass = require('sass'),
    uglify = require("uglify-js"),
    path = require("path"),
    browserSync = require("browser-sync").create();
const ROOT_DIRNAME = __dirname;
const PROJECT_NAME = "aleksandra-dev";

let promisify = (func) => {
    return (...arg) => {
        return new Promise((resolve, reject) => {
            func(...arg, (error, data) => {
                if (error) {
                    return reject(error);
                }
                resolve(data);
            })
        })
    }
};

(function build(argv) {
    (async (argv) => {
        try {
            await createFolders();
            await compileHTML(argv);
            await compileCSS(argv);
            await moveImg();
            await compileJS();
            if (argv === "dev") {
                console.log(argv);
                await startServer();
            }
        } catch (e) {
            console.log(e);
        }
    })(argv);
})(process.argv[2]);

function startServer() {
    browserSync.watch([`./app/styles/*.scss`, `./app/pages/**/style.scss`, `./app/blocks/**/style.scss`, `./app/components/**/style.scss`], compileCSS).on('change', browserSync.reload);
    browserSync.watch([`./app/pages/**/*.ejs`, `./app/blocks/**/index.ejs`, `./app/components/**/index.ejs`], compileHTML).on('change', browserSync.reload);
    browserSync.watch(`./app/img/**`, moveImg).on('change', browserSync.reload);
    browserSync.watch(`./app/pages/*.json`, compileHTML).on('change', browserSync.reload);
    browserSync.watch([`./app/pages/**/script.js`, `./app/blocks/**/script.js`, `./app/components/**/script.js`], compileJS).on('change', browserSync.reload);


    browserSync.init({
        server: {
            baseDir: 'public',
            index: "index.html"
        }
    });
}

async function compileHTML(argv) {
    try {
        let pages = await promisify(fs.readdir)(`${ROOT_DIRNAME}/app/pages`);
        console.log(pages);
        for (let i = 0; i < pages.length; i++) {
            let files = await promisify(fs.readdir)(`${ROOT_DIRNAME}/app/pages/${pages[i]}`);


            let jsonName = files.filter(item => item.toString().indexOf(".json") !== -1);
            let ejsName = files.filter(item => item.toString().indexOf(".ejs") !== -1);
            if (ejsName.length) {
                let object = {};

                for (let j = 0; j < jsonName.length; j++) {
                    object = JSON.parse(await promisify(fs.readFile)(`${ROOT_DIRNAME}/app/pages/${pages[i]}/${jsonName[j]}`, "utf-8"));
                    let html = await promisify(ejs.renderFile)(`${ROOT_DIRNAME}/app/pages/${pages[i]}/${ejsName[0]}`, object, {});
                    if (argv === "git") {
                        html = html.replace(/\/assets\//g, `/${PROJECT_NAME}/assets/`);
                        html = html.replace(/\/#/g, `/${PROJECT_NAME}/#`);
                    }

                    let href = object.href === "" ? "index" : object.href;

                    await promisify(fs.writeFile)(`${ROOT_DIRNAME}/public/${href}.html`, html);
                }
                if (jsonName.length === 0) {
                    let html = await promisify(ejs.renderFile)(`${ROOT_DIRNAME}/app/pages/${pages[i]}/${ejsName[0]}`, object, {});
                    if (argv === "git") {
                        html = html.replace(/\/assets\//g, `/${PROJECT_NAME}/assets/`);
                    }
                    await promisify(fs.writeFile)(`${ROOT_DIRNAME}/public/${pages[i]}.html`, html);
                }
            }

        }
    } catch (e) {
        console.error(e)
    }
}

function compileCSS(argv) {
    (async () => {
        try {
            let css = await sass.renderSync({file: `${ROOT_DIRNAME}/app/styles/styles.scss`,outputStyle: "compressed"}).css.toString();
            if (argv === "git") {
                css = css.replace(/\/assets\//g, `/${PROJECT_NAME}/assets/`);
            }
            await promisify(fs.writeFile)(`${ROOT_DIRNAME}/public/assets/css/style.css`, css);
        } catch (e) {
            console.error(e);
        }
    })();

}

async function createFolders() {
    try {
        try {
            await promisify(fs.readdir)(`${ROOT_DIRNAME}/public`);
        } catch (e) {
            await promisify(fs.mkdir)(`${ROOT_DIRNAME}/public`);
            await promisify(fs.mkdir)(`${ROOT_DIRNAME}/public/assets`);
            await promisify(fs.mkdir)(`${ROOT_DIRNAME}/public/assets/css`);
            await promisify(fs.mkdir)(`${ROOT_DIRNAME}/public/assets/js`);
            await promisify(fs.mkdir)(`${ROOT_DIRNAME}/public/assets/img`);
            await promisify(fs.mkdir)(`${ROOT_DIRNAME}/public/assets/fonts`);
        }
    } catch (e) {
        console.log(e);
    }
}

async function compileJS() {
    try {
        await promisify(fs.writeFile)(`${ROOT_DIRNAME}/public/assets/js/script.js`, "");

        let resultPath = `${ROOT_DIRNAME}/public/assets/js/script.js`;

        let components = await promisify(fs.readdir)(`${ROOT_DIRNAME}/app/components`);
        let blocks = await promisify(fs.readdir)(`${ROOT_DIRNAME}/app/blocks`);
        let pages = await promisify(fs.readdir)(`${ROOT_DIRNAME}/app/pages`);

        let folders = {components, blocks, pages};
        for (let key in folders) {
            let currentArray = folders[key];
            for (let folder of currentArray) {
                await readDir({
                    path: `${ROOT_DIRNAME}/app/${key}/${folder}`,
                    resultPath: resultPath,
                    type: "js"
                })
            }
        }

        let uglified = uglify.minify(await promisify(fs.readFile)(resultPath, "utf-8"));

        // console.log(await promisify(fs.readFile)(resultPath, "utf-8"));
        await promisify(fs.writeFile)(`${ROOT_DIRNAME}/public/assets/js/mscript.min.js`, uglified.code);
    } catch (e) {
        console.error(e);
    }
}

async function readDir({path, resultPath, type}) {
    // console.log("read dir", path);
    let items = await promisify(fs.readdir)(path);
    for (let item of items) {
        let isFolder = (await promisify(fs.stat)(`${path}/${item}`)).isDirectory();
        if (isFolder) {
            await readDir({
                path: `${path}/${item}`,
                resultPath: resultPath,
                type: type
            });
        } else {
            await readFile({
                path: `${path}/${item}`,
                item: item,
                name: item,
                type: type,
                resultPath: resultPath
            })
        }
    }
}

async function readFile({path, item, type, resultPath}) {

    if (item.indexOf(type) !== -1 && item.indexOf("json") === -1 && item.indexOf("ejs") === -1) {
        console.log("write file", path);
        let content = await promisify(fs.readFile)(path, "utf-8");
        await promisify(fs.appendFile)(resultPath, content);
    }
}


async function moveImg() {
    try {
        let images = await promisify(fs.readdir)(`${ROOT_DIRNAME}/app/img`);
        for (let i = 0; i < images.length; i++) {
            await promisify(fs.copyFile)(path.join(ROOT_DIRNAME, `/app/img/${images[i]}`), path.join(ROOT_DIRNAME, `/public/assets/img/${images[i]}`));
        }
    } catch (e) {
        console.error(e);
    }
}
